#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Ce module est une application graphique pygame."""

from pgutil.pygame_application import PygameApplication
from robot.robot_escape_scene import RobotEscapeScene, RobotEscapeSceneSolution

if __name__ == '__main__':
    app = PygameApplication()
    app.run(RobotEscapeScene())
    #app.run(RobotEscapeSceneSolution())
