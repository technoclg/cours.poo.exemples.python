#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Ce module est une application graphique pygame simple."""

import pygame.draw as pgdr

from pgutil.pygame_application import PygameApplication
from pgutil.pygame_scene import PygameScene
from pygame import Color

# Définir ici la classe SimpleCircle

class SimpleCircleSolution(PygameScene):
    def draw(self, surface):
        """Dessine la scène."""
        pgdr.circle(surface, Color("red"), surface.get_rect().center, 50)

def main_solution():
    app = PygameApplication()
    app.run(SimpleCircleSolution())

if __name__ == '__main__':
    # ajouter le programme principal
    pass
