# -*- coding: utf-8 -*-

"""Ce module affiche un joueur Tux."""

import pygame as pg


# Complétez la classe Joueur
class PlayerSprite(pg.sprite.Sprite):
    """Représente Tux contrôlé par le joueur."""

    def __init__(self):
        super().__init__()


class PlayerSpriteSolution(pg.sprite.Sprite):
    """Représente Tux contrôlé par le joueur."""

    IMG_PATH = "robot/resources/images/tux.png"
    SPEED = 5

    def __init__(self):
        super().__init__()
        self.image = pg.image.load(self.IMG_PATH)
        self.rect = self.image.get_rect()
        self.position = (100, 100)
        self.move = (0, 0)

    def update(self, deltat):
        self.position = (self.position[0] + self.move[0] * self.SPEED, self.position[1] + self.move[1] * self.SPEED)
        self.move = (0, 0)
        self.rect.center = self.position

    def up(self):
        self.move = (self.move[0], self.move[0] - 1)

    def right(self):
        self.move = (self.move[0] + 1, self.move[0])

    def down(self):
        self.move = (self.move[0], self.move[0] + 1)

    def left(self):
        self.move = (self.move[0] - 1, self.move[0])
