# -*- coding: utf-8 -*-

"""Ce module affiche un joueur robot."""

import pygame as pg


# Complétez la classe Robot
class RobotSprite(pg.sprite.Sprite):
    """Représente un robot."""

    def __init__(self):
        super().__init__()
        self.image = None
        self.rect = None
        self.position = None
        self.direction = None

    def update(self, deltat):
        pass


class RobotSpriteSolution(pg.sprite.Sprite):
    """Représente un robot."""

    IMG_PATH = "robot/resources/images/robot.png"
    SPEED = 5

    def __init__(self):
        super().__init__()
        self.image = pg.image.load(self.IMG_PATH)
        self.rect = self.image.get_rect()
        self.position = (0, 300)
        self.direction = 1

    def update(self, deltat):
        surface = pg.display.get_surface()
        abscisse = self.position[0]
        if abscisse > surface.get_width():
            self.direction = -1
        elif abscisse <= 0:
            self.direction = 1
        self.position = (abscisse + self.direction * self.SPEED, self.position[1])
        self.rect.center = self.position
