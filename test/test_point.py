import unittest
from coo import point2d

class PointTestCase(unittest.TestCase):
    def setUp(self):
        self.p = point2d.Point2D(1.0, 2.0)

    def assert_point(self, expectedX, expectedY):
        self.assertEqual(self.p.x, expectedX)
        self.assertEqual(self.p.y, expectedY)

    def test_constructor(self):
        self.assert_point(1.0, 2.0)

    def test_str(self):
        self.assertEqual(str(self.p), "Point2D(1.0, 2.0)")

    def test_translate(self):
        self.p.translate(2.0, 3.0)
        self.assert_point(3.0, 5.0)

if __name__ == '__main__':
    unittest.main()
